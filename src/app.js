import hydraCanvas from './components/hydra';
// import isIterable from "./utils/isIterable";
// import '/node_modules/hydra-synth/dist/hydra-synth.js';

import "./styles/style.css";
//parche hardcodeado
//en un futuro armar un fork con un build tipico de lib

import { Camera } from '@mediapipe/camera_utils';
import "@mediapipe/control_utils";
import { drawConnectors, drawLandmarks } from '@mediapipe/drawing_utils';
import { Pose, NormalizedLandmark, NormalizedLandmarkList, POSE_CONNECTIONS, Results } from '@mediapipe/pose';

//const start = Date.now();
//en caso de necesitar un tic sobre hydra o multiples instancias podria agregar un context?

const container = document.createElement("DIV");


//basicamente podria hacerlo con un retaro el get elementeByClass pero bueno esto es lo primero q logre
const videoElement = document.createElement("VIDEO");
const canvasElement = document.createElement("CANVAS");
videoElement.className = "input_video";
videoElement.style.display = "none";

canvasElement.className = "output_canvas";

canvasElement.width = 1280 / 2.;
canvasElement.height = 720 / 2.;

// canvasElement.width= window.innerWidth/2.;
// canvasElement.height= window.innerHeight/2.;

const ctx = canvasElement.getContext('2d');

container.className = "container";
container.appendChild(videoElement);
container.appendChild(canvasElement);

document.body.appendChild(container);

document.body.appendChild(hydraCanvas());
const landmarkContainer = document.getElementsByClassName('landmark-grid-container')[0];
// const grid = new LandmarkGrid(landmarkContainer);

function onResults(results) {
  // console.log(results);
  const width = ctx.canvas.width
  const height = ctx.canvas.height

  ctx.clearRect(0, 0, width, height);
  ctx.resetTransform()
  //las transform tienen este orden :lolo
  // ctx.translate(-width/2, 0)
  // ctx.scale(-1, 1)
  // ctx.translate(width/2, 0)

  ctx.scale(-1, 1)

  ctx.translate(-width, 0)


  // ctx.restore();
  // ctx.scale(-1, 1)

  //  ctx.translate(-width, 0)
  ctx.drawImage(results.image, 0, 0, width, height)

  if (results.poseLandmarks != null) {
    const landmarks = results.poseLandmarks;
    // ctx.save();
    // ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    // ctx.drawImage(results.segmentationMask, 0, 0,
    //                     canvasElement.width, canvasElement.height);

    // // Only overwrite existing pixels.
    // ctx.globalCompositeOperation = 'source-in';
    // ctx.fillStyle = '#00FF00';
    // ctx.fillRect(0, 0, canvasElement.width, canvasElement.height);

    // // Only overwrite missing pixels.
    // ctx.globalCompositeOperation = 'destination-atop';
    // ctx.drawImage(
    //     results.image, 0, 0, canvasElement.width, canvasElement.height);

    // ctx.globalCompositeOperation = 'source-over';
    // drawConnectors(ctx, results.poseLandmarks, POSE_CONNECTIONS,
    //                {color: '#00FF00', lineWidth: 4});
    // drawLandmarks(ctx, results.poseLandmarks,
    //               {color: '#FF0000', lineWidth: 2});

    // ctx.save()

    drawConnectors(ctx, landmarks, POSE_CONNECTIONS, { color: '#00FF0F', lineWidth: 3 })
    drawLandmarks(ctx, landmarks, { color: '#FF0F00', lineWidth: 1, radius: 3 })
    landmarks.forEach((landmark, index) => {


      ctx.resetTransform()
      if (landmark.visibility != null && landmark.visibility > .75) {
        //	console.log(index); 
        //console.log(landmark);
      }
      ctx.fillText(index.toString(), (1. - landmark.x) * width, landmark.y * height);

      //let text = landmark.x.toPrecision(3).toString()+ " , "+landmark.y.toPrecision(3).toString()+ " , "+landmark.z.toPrecision(3).toString();
      //ctx.fillText(text,(1.-landmark.x)*width,landmark.y*height);
    })

    for (let i = 0; i < 3; i++) {
      ctx.beginPath();
      ctx.moveTo(width * (i / 3), 0);
      ctx.lineTo(width * (i / 3), height);
      ctx.stroke();
    }

    for (let i = 0; i < 3; i++) {
      ctx.beginPath();
      ctx.moveTo(0, height * (i / 3));
      ctx.lineTo(width, height * (i / 3));
      ctx.stroke();
    }

    // ctx.translate(width, 0)

    // ctx.resetTransform()
    var valor = "gradient"

    if (landmarks[16].x * width < (width * (1. / 3.))) {
      valor = "osc"
    } else if (landmarks[16].x * width < (width * (2. / 3.))) {
      valor = "noise"
    }

    //voy a tener qu construir un dict de func o varios segu nel caso


    //dividir generadores??
    const source = {
      "noise": (a = 20, b = .1, c = 0) => noise(a, b, c),
      "voronoi": (a = 20, b = .1, c = 0.5) => voronoi(a, b, c),
      "osc": (a = 20, b = .1, c = 0) => osc(a, b, c),
      "shape": (a = 20, b = .1, c = 0) => shape(a, b, c),
      "gradient": (a = 20, b = .1, c = 0) => gradient(a, b, c),
      "src": (a = "o0") => src(a),
      "solid": (a = 20, b = .1, c = 0) => solid(a, b, c),
      "prev": (a = 20, b = .1, c = 0) => prev(a, b, c),
    }
    const geometria = {
      "rotate": (a, b = .1, c = 0) => a.rotate(b, c),
      "scale": (a = 20, b = .1, c = 0) => scale(a, b, c),
      "pixelate": (a = 20, b = .1, c = 0) => pixelate(a, b, c),
      "repeat": (a = 20, b = .1, c = 0) => repeat(a, b, c),
      "repeatX": (a = 20, b = .1, c = 0) => repeatX(a, b, c),
      "repeatY": (a = 20, b = .1, c = 0) => repeatY(a, b, c),
      "kaleid": (a = 20, b = .1, c = 0) => kaleid(a, b, c),
      "scroll": (a = 20, b = .1, c = 0) => scroll(a, b, c),
      "scrollX": (a = 20, b = .1, c = 0) => scrollX(a, b, c),
      "scrollY": (a = 20, b = .1, c = 0) => scrollY(a, b, c),
    }
    const col = {
      "posterize": (a = 20, b = .1, c = 0) => posterize(a, b, c),
      "shift": (a = 20, b = .1, c = 0) => shift(a, b, c),
      "invert": (a = 20, b = .1, c = 0) => invert(a, b, c),
      "contrast": (a = 20, b = .1, c = 0) => contrast(a, b, c),
      "brightness": (a = 20, b = .1, c = 0) => brightness(a, b, c),
      "luma": (a = 20, b = .1, c = 0) => luma(a, b, c),
      "thresh": (a = 20, b = .1, c = 0) => thresh(a, b, c),
      "color": (a = 20, b = .1, c = 0) => color(a, b, c),
      "saturate": (a = 20, b = .1, c = 0) => saturate(a, b, c),
      "hue": (a = 20, b = .1, c = 0) => hue(a, b, c),
      "colorama": (a = 20, b = .1, c = 0) => colorama(a, b, c),
    }
    // "sum":(a = 20, b = .1, c = 0) =>  (a, b, c),
    // "r":(a = 20, b = .1, c = 0) =>  (a, b, c),
    // "g":(a = 20, b = .1, c = 0) =>  (a, b, c),
    // "b":(a = 20, b = .1, c = 0) =>  (a, b, c),
    // "a":(a = 20, b = .1, c = 0) =>  (a, b, c),

    const fusion = {
      "add": (a, b, n = 1) => a.add(b),
      "sub": (a, b, n = 1) => a.sub(b),
      "layer": (a, b, n = 1) => a.layer(b),
      "blend": (a, b, n = 1) => a.blend(b),
      "mult": (a, b, n = 1) => a.mult(b),
      "diff": (a, b, n = 1) => a.diff(b),
      "mask": (a, b, n = 1) => a.mask(b),
    }
    const modul = {
      "modulateRepeat": (a, b, n = 1) => a.modulateRepeat(b),
      "modulateRepeatX": (a, b, n = 1) => a.modulateRepeatX(b),
      "modulateRepeatY": (a, b, n = 1) => a.modulateRepeatY(b),
      "modulateKaleid": (a, b, n = 1) => a.modulateKaleid(b),
      "modulateScrollX": (a, b, n = 1) => a.modulateScrollX(b),
      "modulateScrollY": (a, b, n = 1) => a.modulateScrollY(b),
      "modulate": (a, b, n = 1) => a.modulate(b),
      "modulateScale": (a, b, n = 1) => a.modulateScale(b),
      "modulatePixelate": (a, b, n = 1) => a.modulatePixelate(b),
      "modulateRotate": (a, b, n = 1) => a.modulateRotate(b),
      "modulateHue": (a, b, n = 1) => a.modulateHue(b),
    }

    const dict = {
      "osc": (a = 20, b = .1, c = 0) => osc(a, b, c),
      "noise": (a = 20, b = .1) => noise(a, b),
      "gradient": (a = 20, b = .1) => gradient(),
      "shape": (a = 8, b = .1, c = .5) => shape(a, b),
      "diff": (a, b, n = 1) => a.diff(b),
      "add": (a, b, n = 1) => a.add(b),
      "blend": (a, b, n = 1) => a.blend(b),
    }

    // var listFuncHydra = [];
    //    osc(40, 0.1, 1.2).rotate(Math.PI * landmarks[0].x).out()
    //inicio de cadena de func //esta var y listado deberian ser globales
    var sumFunc = dict[valor]();//osc();

    var listado = ["osc", "noise", "shape"] //agregar un timmer ya sea calsico o con callback
    var otraLista = ["add", "blend", "diff"] //solo de prueba lo ideal seria un json x cada conjunto?
    //cuestion a resolver, pasar un json y desarmarlo con los paramaetros
    //generar un metodo para substraer elementos a listado
    // console.log(listado)
    //este bucle debendra ensamblador de func
    for (var i = 0; i < listado.length; i++) {
      sumFunc = dict[otraLista[i]](sumFunc, dict[listado[i]](4, 0.1,));
      // sumFunc = func["diff"](sumFunc, dict[listado[i]](4, 0.1,));
    }


    class ObjHydra {
      //GeneratorFactory //line 1396
      //podria ver de tomar una herencia? y agregarle los metodos de dict?
      //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Getter_only
      constructor(nodo) {
        this.dict = {
          "osc": (args) => osc(args),
          "noise": (args) => noise(args),
          "rotate": (args) => this.cadena.rotate(args),
          "pixelate": (args) => this.cadena.pixelate(args),
         // "out": (arg) => this.cadena.out(),
         //el out se rompe 
        }
        this.cadena = dict[nodo]();

      }
      get val() {
        // console.log(this.cadena)
        return this.cadena;
      };
      ensamblador(name, args) {
        this.dict[name](args);
        return this;
      }
      out(){
        console.log("se supone q terminaria")
        this.cadena.out(o0);
        console.log("no se rompio!")

      }
    }


    // function concatenarRecussion(anterior,interior) ??
    var test = new ObjHydra("osc");
    test.ensamblador("pixelate").ensamblador("rotate");
    //test.out()//.ensamblador("out")
    // var cierre = test.val;
    // cierre.out();
    //.out();
   
    //resultante
   // var test = new ObjHydra("source", source["voronoi"], [[50, 25, 10], .1, 5]);
    // var rot = new ObjHydra("geometria", rotate(Math.PI * landmarks[0].x));
    // var rot = new ObjHydra("geometria", geometria["rotate"],Math.PI * landmarks[0].x);
    sumFunc.rotate(Math.PI * landmarks[0].x).out()
    // test.get().rotate(Math.PI * landmarks[0].x).out(); //
    // geometria["rotate"](test.get(),Math.PI * landmarks[0].x).out();
    // rot.set(test.get()).out();

  }
}

const pose = new Pose({
  locateFile: (file) => {
    return `https://cdn.jsdelivr.net/npm/@mediapipe/pose/${file}`;
  }
});
pose.setOptions({
  modelComplexity: 1,
  smoothLandmarks: true,
  enableSegmentation: true,
  smoothSegmentation: true,
  minDetectionConfidence: 0.5,
  minTrackingConfidence: 0.5
});
pose.onResults(onResults);

const camera = new Camera(videoElement, {
  onFrame: async () => {
    await pose.send({ image: videoElement });
  },
  width: 1280,
  height: 720
});
camera.start();

  // noise().mult(osc(3.5, .01, 4.5).rotate([Math.PI / 3], 0.951).saturate(20).contrast(5)).modulate(voronoi(10., 0.1, 0), 1.5)
  //   .mult(shape(7, .0275, 1.5))
  //   .mult(osc()
  //     .thresh(.5, 1)
  //     .modulate(noise(), -.025))
  //   // .colorama(50000)
  //   .blend(src(o0)
  //     .modulateHue(src(o0), 10)
  //     .scale(1.01) //1.001
  //     .contrast(1.005), () => Math.sin(time * 0.001) * 0.05 + 0.95)
  //   .hue(.5)
  //   .out()

