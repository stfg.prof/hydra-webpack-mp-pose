import  '/node_modules/hydra-synth/dist/hydra-synth.js';
export default ()=>{
const hydraCanvas = document.createElement("CANVAS");
// ctx=canvas.getContext("2d");
hydraCanvas.width= window.innerWidth/2.;
hydraCanvas.height= window.innerHeight/2.;
var opt= {
    canvas: hydraCanvas, // canvas element to render to. If none is supplied, a canvas will be created and appended to the screen
  
    // width:window.innerWidth,// defaults to canvas width when included, 1280 if not
  
    // height: window.innerHeightnoise,// defaults to canvas height when included, 720 if not
  
    autoLoop: true, // if true, will automatically loop using requestAnimationFrame.If set to false, you must implement your own loop function using the tick() method (below)
  
    makeGlobal: true, // if false, will not pollute global namespace (note: there are currently bugs with this)
  
    detectAudio: false, // recommend setting this to false to avoid asking for microphone
  
    numSources: 4, // number of source buffers to create initially
  
    numOutputs: 4, // number of output buffers to use. Note: untested with numbers other than 4. render() method might behave unpredictably
  
    extendTransforms: [], // An array of transforms to be added to the synth, or an object representing a single transform
  
    precision: null,  // force precision of shaders, can be 'highp', 'mediump', or 'lowp' (recommended for ios). When no precision is specified, will use highp for ios, and mediump for everything else.
  
    pb : null, // instance of rtc-patch-bay to use for streaming
  }

//   document.querySelector("#app").appendChild(canvas);

    const hydra = new Hydra(opt)

  
    return hydraCanvas;

  };
  